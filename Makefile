
# Markdown to docx makefile


# % \usepackage{color}


MASTER_TEX_FILE=thesis

.DEFAULT_GOAL := all
PANDOC=pandoc
PDFLATEX=../pdflatex_docker.sh pdflatex
BIBTEX=../pdflatex_docker.sh bibtex
# PDFLATEX=pdflatex
# PANDOC=scholdoc
RM=rm
SOURCE_DOCS=$(wildcard src/*.tex)
EXPORTED_DOCS=$(SOURCE_DOCS:src/%.md=build/%.tex)

# Build .tex files from markdown source
build/%.tex : src/%.md
	echo $< to $@
	$(PANDOC) $< \
		 --latex-engine=xelatex \
		 --bibliography=src/thesis.bib \
		 --natbib \
		 --smart \
	   --verbose \
		 --top-level-division=chapter \
		 -o $@

	# and now convert the annoying natbib \citep to latex \cite
	sed -i '.bak' 's/\\cite[t,p]{/\\cite{/g' $@
	sed -i '.bak' 's/{natbib}/{cite}/' $@

	# lib/split_preamble.js $@ 0 >> build/scholarly_preamble.tex
	# lib/split_preamble.js $@ 1 > build/temp.tex
	# cp build/temp.tex $@

# Build pdf from .tex files.
dist/thesis.pdf : $(SOURCE_DOCS)
	# First, copy everything into the build directory
	mkdir -p build/ dist/
	cp lib/* build/
	cp src/*.tex build/
	cp src/results_data/*.tex build/
	cp src/*.bib build/
	cp -r figs build/figs

	# Then build it using latex
	# note the semicolons between commands. This keeps the script in the directory.
	# initial compile
	# compile bibliography
	# puts bibliography into document
	# updates reference numbers
	cd build/; \
	$(PDFLATEX) $(MASTER_TEX_FILE); \
	$(BIBTEX) $(MASTER_TEX_FILE); \
	$(PDFLATEX) $(MASTER_TEX_FILE); \
	$(PDFLATEX) $(MASTER_TEX_FILE); \
	cp thesis.pdf ../dist/downs_thesis_draft_`date +%d%b`.pdf; \
	cp thesis.pdf ../dist/17Mar_Downs_Justin.pdf



.PHONY: all clean

all : $(EXPORTED_DOCS) dist/thesis.pdf

clean:
	- $(RM) -rf build/*

Please see `dist/thesis.pdf` to find the most current pdf of my thesis.

Please see the `src` directory if you would like to see my markdown drafts.
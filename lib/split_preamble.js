#!/usr/bin/env node

var fs = require('fs');


var INPUT_FILE = process.argv[2];
var SPLIT_LOCATION = process.argv[3];


var contents = fs.readFileSync(INPUT_FILE).toString()
                 .replace("\\end{document}","")
                 // .replace("\\bibliography{src/thesis}","")
                 .replace("\\documentclass[]{article}","")
                 .replace("\\usepackage[usenames,dvipsnames]{color}","")
                 .replace("\\usepackage[utf8]{inputenc}","")
                 .replace("\\usepackage{subfig}","")
                 // .replace("\\begin{figure}","\\begin{figure}[fragile]")
                 .split("%% ===== Begin LaTeX file ===========================")[1]
                 .split("\\begin{document}");



console.log(contents[SPLIT_LOCATION]);


## Object Tracking and optical flow

optical flow is the general term used to describe how entities move in a scene. Object tracking specifically refers to tracking a discrete object as it moves from frame to frame. While these concepts are related, object tracking always requires a detection step. Sometimes this step is performed by an algorithm although it is commonly performed by user-input.

The Lucas-Kanade Tracking Algorithm [@LucasKanade] tracks optical flow and a template’s transformation through time. The LKT algorithm requires a template to start with.
The Continuously Adaptive Mean Shift (CAMSHIFT) algorithm [@camshift] uses a mean-shift algorithm to track a cluster center, updating its solution every frame.

Particle filtering [@particle_filtering] and Kalman Filtering [@kalman_filtering] are statistical algorithms that use Bayesian inference to predict state based on past observations, accounting for observation inaccuracies. Particle Filtering is a discrete version of Kalman Filtering and is used in particular to deduce optical flow.

Contour Tracking [@contour_tracking] is an algorithm closely related to clustering that calculates an outline of an object, which the authors call a “contour.” This outline-based representation of an object is able to better cope with rapid, non-affine transformations as well as occlusions.





for d in */ ; do
  for e in $d/*/ ; do
    echo "$e"
  done
done

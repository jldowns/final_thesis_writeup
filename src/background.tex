\chapter{Background}\label{ch:background}

  The problem of object detection is central to computer vision and is the foundation upon which other computer vision problems rely. Before a system can classify an object, it must first know that an object exists at all. Detecting an object in a single image is often easy; photographs are usually taken so that a single ``dominant object'' is present and obvious. Many of the techniques described in this chapter are designed to operate on a single image. Video sequences, however, provide additional information that can improve detections. How an algorithm processes this additional temporal information can be categorized as either \emph{detect-first} or \emph{track-first}.

  A detect-first approach tries to detect objects in each frame individually and then combine the detections to strengthen the predictions. A spurious false-positive in one frame can be thrown out if adjacent frames do not find an object nearby. The advantage of detect-first algorithms is that they can utilize algorithms designed for single images which are often more mature and reliable than algorithms designed for video.

  A track-first approach analyzes the differences between adjacent frames and extracts temporal information first (such as optical flow), then makes detections based on that information. A dark shape may not look like an object in a single frame, but if over several frames it moves in a different direction than the rest of the scene it can be separated from the background and classified as an object. While more work has to go in to developing algorithms designed specially for video sequences, track-first approaches can better utilize the temporal information present in video.


\section{Traditional Object Detection}\label{sec:traditional-object-detection}

  Background Subtraction \cite{background_subtraction} is a very simple example of a track-first algorithm. As its name implies, a ``background image'' is subtracted from an image under investigation in order to find a difference. If the background image is sufficiently representative of the scene being analyzed, the difference between the two images will be any objects not part of the background, or put another way, the foreground. Figure \ref{fig:background_sub} illustrates a simple version of this concept.


  \begin{figure}
    \centering
      \includegraphics[width=0.3\textwidth]{figs/background/background_subtraction/img1.jpg}
      \includegraphics[width=0.3\textwidth]{figs/background/background_subtraction/img2.jpg}
      \includegraphics[width=0.3\textwidth]{figs/background/background_subtraction/sub.jpg}
    \caption{Background Subtraction. Adapted from \cite{visor_dataset}.}
    \caption*{\small
    The left image is the background image, while the center image is under investigation. Subtracting the pixel values of each image results in the image on the right.
    }
    \label{fig:background_sub}
  \end{figure}


  While this technique has the advantage of being simple, it requires that a background model be defined. The most basic solution is to provide the algorithm with an image of the scene with no foreground objects. But this solution is rarely effective: a slight movement of the camera will misalign the background model and result in the wrong pixels being subtracted from each other (see Figure \ref{fig:background_sub_shift}). This solution does not account for lighting changes or object movement: if a person moves a book a few inches and leaves, the book will be highlighted until a new background model is chosen. Furthermore, this technique assumes that an image with no foreground objects is available. This might not be the case for a busy scene.

  \begin{figure}
    \centering
      \includegraphics[width=0.3\textwidth]{figs/background/background_subtraction/sub_20_pixel_shift.jpg}
    \caption{Background Subtraction with a 20 pixel shift of the background. Adapted from \cite{visor_dataset}.}
    \caption*{\small
    If the background image is shifted only slightly, the difference between the two images includes more than just foreground objects and becomes less useful for object detection tasks.
    }
    \label{fig:background_sub_shift}
  \end{figure}

  Most implementations are more sophisticated and continuously update a background model by keeping a recent history of images. While there are many ways of creating this model \cite{background_roundup}, they suffer from the limitation of only ``seeing'' moving objects. After a stationary object is introduced to the scene it will initially be detected but slowly fade into the background as long as it remains stationary.

  Template matching was another early (and still commonly used) technique to detect objects in a scene. Template matching algorithms seek to develop a model for the object they are designed to detect, and then search for regions in an image that match the template. One simple template-matching technique involves simply developing image kernels that look similar to objects of interest. Convolving these kernels with an image results in high responses in regions that look similar to the kernel. This method involves developing kernels for many aspects of each object the detector must operate on. While this method completes both object detection and classification in one step, it is very sensitive to lighting changes, visual obscurations, and unexpected transformations. Furthermore, a template must be developed in advance for every object that the algorithm is designed to detect; a template matching algorithm must either be extremely generic (such as Haar Wavelets \cite{haarwavelets}) or extremely composable (such as part-based hierarchical template matching \cite{part-based-template-matching}) to be useful in practice (see Figure \ref{fig:template_matching}).

  \begin{figure}
    \centering
      \includegraphics[width=0.45\textwidth]{figs/background/template_matching/wavelet1.png}
      \includegraphics[width=0.45\textwidth]{figs/background/template_matching/part_based1.png}
    \caption{Template matching with Haar Wavelets (left) and a Hierarchical Part-Based Approach (right). Source: \cite{haarwavelets}, \cite{part-based-template-matching}.}
    \label{fig:template_matching}
  \end{figure}



  Another template-matching approach is Histogram of Oriented Gradients (HOG) feature matching (with SVM classification) \cite{hog_feature_matching}. Instead of relying on convolutional kernels to detect and classify objects, HOG feature matching creates a ``histograms of gradients feature vector.'' Histograms of gradients are intended to be more general than strict pixel comparisons by representing a template not in pixels but by the relationships between object edges. (Scale-invariant feature transform, or SIFT, is another way of generating a feature vector from a subimage \cite{sift}.) This feature vector achieves rotational, lighting, and size invariance, solving many problems faced by other template-matching approaches. Still, these feature vectors cannot handle occlusions or generalize to recognize shapes that haven't been explicitly introduced to the algorithm. Furthermore, they only operate on a single image; these algorithms by themselves cannot fuse information from multiple frames to improve their performance.


\section{Object Detection in Neural Networks}\label{object-detection-in-neural-networks}


  Until recently, the best general image classification and detection algorithms relied on various forms of the previous techniques. However, in 2012 a team of researchers from the University of Toronto won the \ac{ILSVRC} by utilizing a concept known as ``convolutional neural networks'' \cite{krizhevsky2012imagenet}, \cite{ILSVRC15}. Convolutional neural networks had long been enlisted for simple tasks like processing checks and reading envelopes \cite{lenet}, but improvements in datasets and computational resources have allowed researchers to apply them to more complex problems such as general image classification and object localization. Put simply, a neural network is a series of linear classifiers, where the output vector of one classifier is fed into the input of the next. By representing data points as a mathematical vector of features, neural networks optimize a set of ``weights'' that can be multiplied with the data to derive in a vector of predictions. In computer vision, the feature vector is usually just the image pixel values. While applying a simple linear classifier to pixel values would seem to be a naive approach to image classification, chaining multiple classifiers together allows each ``layer'' to learn different concepts about images.

  One special-purpose layer used commonly in computer vision is known as a ``convolutional'' layer. Instead of deriving optimal weights to multiply with each data point in the input vector, a convolutional layer derives an image kernel that it convolves with the input vector. By stacking these layers together a convolution neural network effectively implements a template-matching approach to recognize objects in an image, except that it creates hundreds of general templates and usually stacks multiple convolutional layers together. Early layers usually learn to recognize simple edges and shapes (such as those pictured in Figure \ref{fig:weights}) while later layers synthesize these results to recognize more complex concepts, such as car tires or faces.  An early successful convolutional neural network, called LeNet-5, is shown in Figure \ref{fig:lenet_diagram}.




  \begin{figure}
    \centering
      \includegraphics[width=0.8\textwidth]{figs/background/neural_nets/weights.png}
    \caption{Learned weights from a convolutional layer in AlexNet. Source: \cite{krizhevsky2012imagenet}.}
    \label{fig:weights}
  \end{figure}

  \begin{figure}
    \centering
      \includegraphics[width=0.8\textwidth]{figs/background/neural_nets/lenet5.png}
    \caption{The LeNet-5 convolutional neural network was developed to classify handwritten characters. Source: \cite{lenet}.}
    \label{fig:lenet_diagram}
  \end{figure}


  % But while the resurgence of neural networks has made huge improvements to image classification \cite{krizhevsky2012imagenet}, video classification \cite{yue2015beyondshortsnippets}, and object detection in single images \cite{faster-rcnn}, there have so far been very few efforts to us-e information from multiple video frames to improve object detection performance. The following are several algorithms that solve different but related problems.

  At first, these convolutional neural networks simply provided image classification. The LeNet-5 network, for example, classified input images as one of ten digits. \textit{Localization} is a different task closely linked to object detection. The localization task requires an algorithm to identify the subset of an image in which an object can be found. Put another way, it requires that the \textit{location} of objects within an image are determined. One of the first successful methods developed for localizing objects with neural networks is known as the ``sliding window'' technique, used most effectively by the Overfeat network to win the 2013 Large Scale Visual Recognition Challenge \cite{overfeat_sliding_window}. Instead of simply outputting a classification of the dominant object in an image, the network was equipped to output bounding-box coordinates through regression. Overfeat improved the technique by applying the bounding-box regression and classification to many ``windows'' in an image and aggregating the results (pictured in Figure \label{fig:sliding_window}.) While this technique was successful it was also inefficient: a single image typically required hundreds of passes through the network.

  \begin{figure}
    \centering
      \includegraphics[width=0.60\textwidth]{figs/background/neural_nets/sliding_window.png}
    \caption{Sliding window algorithm developed for the Overfeat network. Source: \cite{overfeat_sliding_window}.}
    \caption*{\small
    In order to localize the bear in the image, the Overfeat network classified thousands of subimages and built a model of what was in the image. A post-processing step combined the classifications and returned a bounding box. This approach to object detection is conceptually simple but time-intensive.
    }
    \label{fig:sliding_window}
  \end{figure}



  The idea of ``region proposal'' was introduced in 2013 as ``selective search,'' \cite{selective_search} an extension of existing bottom-up segmentation techniques. What made selective search novel was that it passed image segments to a classifier ``after'' splitting an image into discrete sections. (The sliding-window technique, as previously explained, classifies every part of an image and localizes objects by finding the classifications with the highest scores.) The selective search algorithm first segments an image using a graph-based approach described by Felzenszwalb and Huttenlocher \cite{graph_based_segmentation}, and passes the segments to a neural network for classification. This method reduces many of the extraneous classifications made by the sliding-window technique but cannot adapt its segmentation algorithm to best fit different datasets. If its segmentation algorithm performs poorly over a dataset, it cannot be improved with additional training. In other words, machine learning starts and stops with the classification.


  % This method reduces many of the extraneous classifications made by the sliding-window technique but us-es its general-purpose segmentation algorithm as a ``gatekeeper'': only likely objects are passed to the neural network for classification. This method is only as good as its segmentation algorithm; detection does not improve with additional training.



  % This method reduces many of the extraneous classifications made by the sliding-window technique but loses many of the advantages gained by neural networks.
  %
  % The general-purpose segmentation algorithm effectively acts as a ``gatekeeper'' and only passes what it considers to be likely objects to the neural network classifier. Since the segmentation algorithm does not
  %
  %
  % This limits the ability of the neural network to perform object detection: it can classify
  %
  %
  % In other words, the machine learning starts and stops with the classification.

  In 2015 Microsoft introduced a neural network capable of proposing regions of interest in its ``Faster R-CNN'' paper \cite{faster-rcnn}. This advance allowed the network to not only more accurately and quickly detect objects in an image (since the network did not require a long pre-precessing stage), it gave the network the capability to ``learn how to better detect objects.'' Instead of relying on a general object-detection or segmentation algorithm to find object-like things, Faster R-CNN learned what ``objects'' in the training dataset looked like. Furthermore, since the network is trained end-to-end, gradients from the classification section of the network flow up through the region-proposal section. When the network gets a classification wrong, not only does the classifier adjust its weights; the region-proposal network also adjusts its weights so that it can propose better regions (regions that will result in better classifications, that is) in the future. This architecture still beats out most others for object detection \cite{ILSVRC15}.


  \section{Object Detection in Video Using Neural Networks}\label{object-detection-in-video-using-neural-networks}

  The idea of feeding multiple frames of data to a neural network is not unprecedented: in 2013 a team of researchers developed a convolutional neural network to classify similar human actions such as jogging, running, handwaving and hand-clapping \cite{human_action_recognition}. These activities are usually impossible to classify without multiple frames of data. The team utilized 3-dimensional convolution layers to learn temporal features and applied the algorithm to busy scenes of people moving. While they were successful in classifying human actions that could not easily be classified in a single frame, their solution was strictly a classifier. They relied on a face-detector to first detect all the humans in a video, then inferred their positions. (See Figure \ref{fig:face_detection}.) After determining the regions of the video that contained humans they passed cropped video sequences to their neural network for classification. Since their 3D convolutional neural network was part of the region proposal process, their algorithm depended on the strength of the face detector; if the face detector failed to detect a human, their 3D convolutional neural network would never have the opportunity to make a classification.


  \begin{figure}
    \centering
      \includegraphics[width=0.45\textwidth]{figs/background/neural_nets/face_detection.png}
    \caption{Detecting human actions in video; face-detection pre-processing step. Source: \cite{human_action_recognition}.}
    \caption*{\small
    Shuiwang Ji et al. used 3D convolutions in their neural network to detect human actions through time. They did not incorporate a region proposal network into their solution, though; a face detector extracted humans from the videos and passed those subimages to the 3D convolutional neural network.
    }
    \label{fig:face_detection}
  \end{figure}


  There have been several projects since that extend the idea of temporal features, introducing techniques such as a peripheral-fovea concept, external visual flow information, and Long-Short Term Memory (LSTM) layers to improve performance. \cite{yue2015beyondshortsnippets}, \cite{karpathy2014large} These papers describe several methods, illustrated in Figure \ref{fig:early_late_fusion}, for combining information across frames for video classification. The first method, called Early Fusion, introduces a higher-dimensional convolutional layer to collect and process information. While a normal CNN employs 3-dimensional kernels (2 dimensions to cover the image height and width and a third to cover each color channel), these algorithms extend the kernels into a fourth dimension which was covered multiple frames. Late Fusion combines information from two frames spaced several frames apart. Slow Fusion employs several locally connected layers to allow the network to slowly combine frame data. All of these projects tackled the problem of \emph{classification}, not region proposal. While it is tempting to apply their results to all neural network video tasks, classification and region proposal are complementary tasks and the interaction between them is not yet sufficiently understood.

  \begin{figure}
    \centering
      \includegraphics[width=0.7\textwidth]{figs/background/neural_nets/early_late_fusion.png}
    \caption{Different approaches for combining information from multiple frames in a neural network. Source: \cite{karpathy2014large}.}
    \caption*{\small
    While these methods are sufficiently general to inform any neural network architecture that combines multiple frames, both papers describing these approaches tested them on whole-frame classification.
    }
    \label{fig:early_late_fusion}
  \end{figure}

  Recurrent networks \cite{recurrent_visual_attention} are a promising alternative to these ``fusion'' approaches. Recurrent networks receive the same information as a traditional network (in the case of imagery, a vector of pixels usually) but keep track of their state between iterations. A specialized neuron called a Long Short-Term Memory neuron \cite{LSTM} allow recurrent networks to selectively ``remember'' details that they deem important, and recall those details in later iterations. Common state information carried forward includes words in a sentence (to predict the next word), the last location analyzed (in the case of the cited work), and the location in a previous frame that an object was found. These networks have the advantage that they do not depend on their architecture to determine how far back their memory goes. If it turns out that it is important to remember a small detail seen 50 frames ago, the Long Short-Term Memory neurons are capable of keeping that information available. The ``fusion'' techniques, on the other hand, require that the number of frames fused together be defined as part of the network's architecture. As exciting as recurrent networks are, however, they have not yet been employed for region-proposal and classification tasks.


  In 2015 and 2016 the \ac{ILSVRC} included an ``object detection from video'' competition alongside its traditional object detection challenge. The competition involved localizing ``dominant objects'' from 30 categories in short video sequences. Most entries followed the same technique: run an object detection algorithm on each frame individually (such as Faster R-CNN \cite{faster-rcnn} or ResNet101 \cite{resnet101}) and aggregate the results across frames. The most successful teams have been those with the most sophisticated post-processing steps. The most recent winning team described a way of aggregating the detections in an algorithm they dubbed ``Tubelets'' \cite{tubelets}. This technique allowed high confidence in one frame to influence the next (since if an object is present in one frame it will probably be present in the next frame) and relied on an external motion model to propagate predictions through time. Still, this technique only provides its region proposal network a single frame of information. These detect-and-aggregate approaches work well for the \ac{ILSVRC} Dataset where there is a dominant object that can easily be detected in a single frame. If their region-proposal networks cannot find the object by looking at a single frame (such as the scenario presented in Section \ref{sec:problem_statement}), their post-processing aggregation step will not be able to compare any information across frames.



\section{Using Multiple Frames in a Region Proposal Network}\label{multiple_frames_region_proposal}

  Instead of following the detect-then-track techniques employed by the latest \ac{ILSVRC} winners, we designed a region proposal network that accepts multiple video frames as input. By giving the neural network direct access to several frames, it has the opportunity to learn temporal features, such as motion and changes in shape. Access to multiple frames also gives it the information necessary to strengthen its detection confidence during especially noisy moments, as described in Section \ref{sec:problem_statement}. We did not conduct any pre-processing except for data normalization and augmentation, nor did we rely on a post-processing step to fuse predictions. We are introducing a completely end-to-end region proposal network that is capable of utilizing temporal information to make object detections. To ensure that the network is in fact using the multiple frames provided to it, we test its performance against an identical network given only a single frame of information. We then test both networks on a new maritime dataset meant to simulate the problems outlined in Section \ref{sec:problem_statement}.



  % While our approach did not follow any specific published technique entirely, it implemented many of the ideas introduced in this chapter. The combination of multiple frames closely followed the Early Fusion technique described by Karpathy et al. \cite{karpathy2014large}, although the sequencing of frames differs from their recommendation. Our region-p

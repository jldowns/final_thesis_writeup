Thank you to my parents for their encouragement and faith that I would be able to finish this
thesis.

I would like to thank Mathias Kolsch for his encouragement and patience that enabled me ¨
to complete this thesis. Thank you for your constant support, available even at odd hours of the
day and weekend.

To Chris Darken, thank for your providing a sounding board and asking thought provoking
questions about this thesis so that I would deliver a good final product.

To Vladimir Dobrokhodov, Kevin Jones, Tim Chung, and Jeff Wurz, thank you for adapting
members of the Rascal fleet and associated software for the TNT experiments. It was a very
rewarding experience to see my efforts actually produce a proof of concept in a real world exercise.

To Justin Jones, my friend and college who helped me get started with OpenCV, annotating
samples, and building detectors.

Thank you to Space and Naval Warfare Systems Center, Pacific, especially my mentor Daniel
Cunningham, whose fellowship grant enable the purchasing of hardware to support the field
experiments in this thesis.



############################################

I would like to thank my advisors for all of their help in developing the ideas
presented in this thesis to the fullest as well as for their assistance in shaping the
development of my academic writing ability.

############################################

The last year has been a roller coaster ride as I began the journey that has culminated in
this thesis. Throughout this journey, I have received immense support from my family,
friends, and NPS professors. Professor Xie, your knowledge and enthusiam for networking
is what steered me towards this topic in the beginning and helped get me through the
detailed understanding and simulation of TCP timestamps and network behavior. Professor
Kolsch, without your guidance my understanding of regression modeling, the AWS cloud,
and (life-saving) automated deployment of instances would not have been anywhere near
the level necessary for this study. You both have been instrumental in my work progression
and for that I cannot thank you enough.

My father, Dr.James Wasek, you provided advice and many last minute critiques asI worked
to make deadlines. You have always been an inspiration to me and I would not be who I
am today without you.

And finally my wife, Sascha, and my children, Zoe and Liam, it is for you that I have
accomplished this feat. You all have sacrificed greatly over the last 18 months while giving
me your full support, providing an ear to vent to, and being my rock to lean on. I thank you
and love you all from the bottom of my heart!

############################################

I would like to thank the many people that have helped me to complete this
research. First is my wife, Carrie, for being so patient and supportive while I spent many
hours in the classroom and in the lab away from home. I would also like to thank my
thesis advisors, Dr. Mathias Kölsch and Dr. Timothy Chung for their enormous
confidence in my abilities, their technical guidance, and their patient support. Without
their inspiration and guiding hands, I would not have completed any of this. I’d like to
thank CDR Matthew Humphries of the NPS Physics department for his assistance with
sensor configuration and maintenance.

Many thanks go to my fellow master’s students working on thesis research in the
NPS Vision Lab: Maj. Brett Lindberg, LT Jason Nelson, LCDR Joshua Burkholder, and
Capt. Justin Jones. The long hours spent in the lab and in the classroom were both
frustrating and rewarding.

############################################

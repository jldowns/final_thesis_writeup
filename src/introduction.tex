\chapter{Introduction}\label{ch:introduction}

  Detecting interesting and suspicious objects in a maritime environment is both extremely important to the United States Navy and very difficult to accomplish well without human involvement. Relying on human experts to detect maritime objects, however, is expensive and often unfeasible. The problem of finding a human survivor in the water is a perfect example where relying on humans for object detection becomes problematic: the rotary and fixed wing platforms employed in typical search and rescue (SAR) have high operating costs and put additional human life at risk. Humans are also notoriously susceptible to fatigue and must limit on-station time to account for human limitations \cite{sar_tacaid}.

  While computer vision algorithms have historically been inferior to human vision capabilities for object detection, the past several years have seen huge leaps in computer vision performance \cite{ILSVRC15}. Many computer vision algorithms have even surpassed human capabilities for some tasks \cite{microsoft_beats_humans}, \cite{skin_legions}. If this trend continues (and there is no sign of it slowing) it is reasonable to expect computer vision to become a viable replacement (or a crucial tool) for humans in many jobs requiring visual analysis.

  The inherent advantages of replacing humans with computers for detection tasks are huge. The high-risk and high-cost human-piloted platforms can be replaced by a larger swarm of low-cost autonomous drones. The on-station time will no longer be dictated by human factors, but instead by the platforms' capabilities. A computer's performance can be much more predictable than a human crew's and will not depend on training differences and personality.


\section{Problem Statement}\label{sec:problem_statement}

% Since we keep mentioning the problem of ``requiring multiple frames for detection,'' the idea deserves some explanation.
  Imagine the task of detecting only moving cars but ignoring stationary cars. An object detector could probably do very well by looking for clues in a single frame of video: cars in parking spots are usually stationary, moving cars may have a motion blur, and if it had an infrared sensor it could even detect hot engines. But such approaches can only be so helpful: some cars stop in the middle of the road, some are moving slow enough that there is no blur, and cars that have parked recently have warm engines. While algorithms can be designed to overcome these obstacles they will not generalize to other tasks, such as detecting pedestrians or butterflies.

  The usefulness of multiple frames does not stop at detecting moving objects, either. Videos are often collected at lower quality than still images, and are often plagued by sensor noise and compression artifacts. Differentiating between a dog and a fox may involve collecting different clues from different video frames. (Perhaps the ears are more visible in one frame and the tail is more visible in a different one.) Multiple frames, then, can conceivably improve classification performance.

  Detecting objects at sea requires overcoming all of these problems. Instead of distinguishing between a dog and fox, a common maritime detection task is to differentiate between a human survivor and a whitecap. This task combines both problems from before: A human survivor and a whitecap look identical, except that a survivor ``remains the same while the whitecaps blink on and off'' \cite{national_sar_sup}. Furthermore, typical surveillance systems have wide angles of view and potential survivors occupy only a small fraction of the entire image (sometimes only a few pixels). The noise and artifacts present in the image make detecting any distinguishing details often impossible with just a single frame. It is clear that if a computer vision algorithm is to satisfactorily detect a human survivor, it must fuse information from multiple video frames.

  Even as researchers develop better algorithms for classification and detection of simple objects, there has not been very much research into the problem of detecting objects in the maritime environment, a problem with unique challenges that must be addressed directly. Most current computer vision benchmarks focus on images with ``dominant objects,'' or objects that occupy a significant fraction of the image \cite{ILSVRC15}, \cite{coco}. (A typical image is shown in Firgure \ref{fig:imagenet_typical}.) Solving the problem of object detection at sea must include developing a dataset that better models the problem.


  \begin{figure}
    \centering
      \includegraphics[width=0.2\textwidth]{figs/intro/imagenet_typical.jpg}
    \caption{Typical Imagenet sample. Source:\cite{ILSVRC15}. }
    \label{fig:imagenet_typical}
  \end{figure}


\section{Research Questions}\label{sec:research_questions}

  Our contribution to the field of object detection is to advance object detection at sea by developing a benchmark that simulates the task of detecting a survivor at sea. This benchmark will encapsulate the problems of
  Since actual SAR sensor footage has not been approved for public release,

  We introduce a method of combining multiple frames of information to improve the performance of a neural network for the task of maritime object detection. Specifically, we attempt to answer:

  \begin{enumerate}

    \item Can providing information from multiple frames of video improve the detection performance of a convolutional neural network compared to an identical network given only a single frame of information? How much of an improvement does adding temporal information provide?

    \item Can a neural network network be designed to reliably detect objects in a maritime environment?

  \end{enumerate}


\section{Thesis Organization}\label{sec:thesis_organization}

  Chapter 2 describes the current landscape of computer vision, with particular attention paid to object detection and neural networks. It explains historical and contemporary approaches to relevant problems and explores the advantages each approach introduced, as well as the pitfalls.

  Chapter 3 describes our experimental setup. The frameworks, architectures, and parameters used during our experiments are explained in depth.

  Chapter 4 lists the results of our exploration in adding multiple frames to convolutional neural networks. We describe the improvements made by adding the multiple frames during training for both an existing benchmark and our new maritime dataset. We pay particular attention to the ways in which the networks fail and how the network is able to leverage the additional frames of information.

  Chapter 5 discusses the possible implications of our results and suggests directions for future research, with an emphasis on the importance and uniqueness of maritime object detection. We also provide some insight and suggestions for driving interest in the problem.

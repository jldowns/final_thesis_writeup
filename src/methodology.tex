\chapter{Methodology}\label{ch:methods}


    We designed a convolutional neural network to test and measure the performance gains of object detection when using multiple frames from a video versus a single frame. We designed a data layer that provided any number of frames of information at an arbitrary frame rate. By changing only the data provided to the network and keeping the rest of the architecture constant we were able to observe how varying the number of input frames affected the performance of the network.

\section{Data Augmentation and Pre-processing}\label{sec:data-augmentation-and-preprocessing}

  In order to prevent the network from overfitting and learning common paths of motion (for example, a commonly used corridor in the 3DPeS dataset) we implemented several data augmentation techniques that are now standard practice in neural network training \cite{data_augmentation}.

  Every series of video frames, before being processed by the network for training, was randomly flipped horizontally or vertically, and/or transposed. Every frame in a 10-frame clip was transformed thew same way. This pre-processing step avoided orientation bias by ensuring an equal chance of orienting the image any of the 8 possible ways to flip and/or rotate an image. Images were not transformed during testing.

  In both the training and the testing steps, a constant was subtracted from each pixel value before providing the image sequence to the network. This constant was derived from the mean value of all the pixel values in the training set and was different for every corpus. While we derived our mean pixel values by analyzing every image in each training corpus (listed in Table \ref{tab:mean_table}), we discovered that the pixel means of our datasets had very low variance. The pixel means for every frame in any given camera angle in the \ac{3DPeS} Dataset had a standard deviation of less than 1.8; the frame means in the Monterey Bay Webcam Dataset had a standard deviation of 4.0. This observation allows future implementations to analyze much fewer training frames to derive a representative mean value. If you can assume that the mean of the frames in a ``surveillance-style'' video corpus (where the videos are captured at a fixed camera angle in consistent lighting) has a standard deviation of 4.0, the mean of a single frame has a 99\% chance of being within 10 of the entire corpus's mean.








  % --- MB --
  % mean: 123.928495713
  % std: 3.9635333107
  % --------------------
  % --- VISOR --
  % mean: 148.004067986
  % std: 22.6699020447
  % --------------------

  \begin{table}[!h]
  \centering
  \begin{tabular}[]{@{}ll@{}}
  \toprule
  Dataset & Constant Mean Value\tabularnewline
  \midrule
  \ac{3DPeS} \cite{visor_dataset} & 160\tabularnewline
  Monterey Bay Webcam & 125\tabularnewline
  \bottomrule
  \end{tabular}
  \caption{Constant mean subtracted from each image in both the training set and the test set, by dataset.}
  \label{tab:mean_table}
  \end{table}


  Since our Monterey Bay Webcam Dataset contained many frames with no object present, we added logic to our data layer to ensure that at least 50\% of the sequences introduced to the network during training contained an object. While this ensured that whole-image classifications would be be split between positive and negative examples, it did not ensure an even split for each \textit{region} of an image. Section \ref{sec:mb_description} goes into more detail about the data imbalance for the Monterey Bay Webcam Dataset.

\section{Network Architecture}\label{network-architecture}

  In order to measure the effects of additional temporal data, we designed two networks and compared their results. The two networks are identical except for the number of frames they accept. Figure \ref{fig:network_diagram} provides an illustration of the general acrchitecture, where \(f\) is the number of frames provided to the network.

  \begin{figure}
    \centering
      \includegraphics[width=1.0\textwidth]{figs/cnn_diagram.png}
    \caption{Convolutional neural network architecture developed for this project.}
    \caption*{\small
    We tested the network with different numbers of frames as input. In every case, \(f\) is the number of frames provided as input.
    }
    \label{fig:network_diagram}
  \end{figure}


  \subsection{Data Input}\label{data-input}

    Our network was loosely based on the well-known LeNet CNN \cite{lenet}. Due to memory constraints, we limited batch size to 1 during training. Since such a small batch size can result in inaccurate estimates of the gradient, we reduced the learning rate to  \(\alpha = 0.0001\) as described in Section \ref{training}.

    The network received image sequences at a dimensionality of \(f \times 300 \times 300\), where \(f\) is the number of frames in the sequence. Each frame, then, contained \(300 \times 300\) pixels and a single grayscale channel.

  \subsection{Single-frame architecture}\label{single_frame_architecture}

    To provide a baseline performance level, we trained the network to identify objects given only a single frame of input. This was achieved by setting our custom data layer to provide a single frame of input, depicted in Figure \ref{fig:network_diagram} where \(f=1\).


  \subsection{Multiple-frame architecture}\label{ten_frame_architecture}

    To test the network's ability to utilize multiple frames of information, we set the data layer to provide 10 frames to the network in chronological order. Empirically, we found the most success when providing the network with 10 frames at a frame rate of 4.8 frames per second. (This is the result of dropping 4 out of every 5 frames in a 24 fps video clip.) The data layer provided the frame data to the network as a \(10 \times 300 \times 300\) array, depicted in Figure \ref{fig:network_diagram} when \(f=10\).

    When training with multiple frames, the network was trained to detect objects located in the 6th frame provided to it. This allowed the network to look 5 frames (0.75 seconds) into the past and 3 frames (0.45 seconds) into the future to assist with detection. While this ``look-ahead'' does introduced a 3-frame ``lag'' into the system, this network architecture can still operate on a ``live'' video feed; albeit with a 0.45 second minimum detection delay.

  \subsection{Ground truth labels}

    The labels were provided at the input layer as an array with a length of \(N^2\). The image was divided into a grid with dimensions \(N \times N\); if a bounding box was present in a specific grid-box, the corresponding label array element was set to 1. If no object bounding box was present in a grid-box, the corresponding label array element was set to 0. (See Figure \ref{fig:classification_metric} for an illustration of how images were divided into grids.) Since bounding boxes did not perfectly segment objects (box corners almost never actually contain the object being annotated), grid-boxes were only labeled as containing a bounding box if a certain percentage of the grid-box overlapped a bounding box. We achieved the best results if we only counted a grid-box as containing a bounding box if at least 3\% of the grid-box's total area was overlapped. We found the best balance of network performance and region-proposal resolution when \(N=6\), resulting in a a 36-element label array.

  \subsection{Convolutional Layers}\label{convolutions-and-pooling}

    \subsubsection{Description}

      Similar to the LeNet model, we followed the data layer with a 20-filter convolutional layer using \(5 \times 5\) pixel filters and a stride of 1.

      We departed from LeNet on the next layer, replacing the max-pooling layer with another convolutional layer. This layer was also 20 filters deep and employed \(5 \times 5\) filters, but compressed the data with a stride of 2.

      The next convolutional layer was identical to the LeNet model: \(5 \times 5\) pixel filters and a depth of 50.

      The colvolutional layers were followed by a pooling layer with a spacial extent of \(20 \times 20\) and a stride of 2. While this is an unusually large pooling extent, this setting achieved a better result than smaller kernel sizes.

    \subsubsection{Motivation}

      While grayscale images have an input depth of 1 and color images have an input depth of 3, our multi-frame network has an input depth of 10. This means that instead of learning normal visual features like those pictured in Figure \ref{fig:weights}, the first convolutional layer will be learning features with 10 channels, where each channel corresponds to a different slice of time (instead of different colors like a typical image). The features learned in this layer will be temporal features, since the convolutions will be performed over several frames. While a convolutional neural network operating on normal 3-channel single color images is able to distinguish when a green area is to the left of a red area, this 10-channel ``temporal'' convolutional layer will be able to learn when an shape in one frame moves to the left in the next frame. This first layer might also learn about how an object changes through time, such as a bird flapping its wings.

      After this first layer learns temporal features, the following convolutional layers operate on these features in more complex ways. The same way that typical convolutional neural networks combine edge detectors features to detect truck wheels and cat ears, a temporal convolutional neural network can combine relative motion (movement through the scene) with shape transformation (a bird flapping its wings) to distinguish a bird flying through the air and a bird flapping its wings stationary in the water.


    \subsection{Fully Connected Layers}

      The result of the max-pooling was fed into a 500-element fully-connected layer using Rectified Linear Units (ReLU) \cite{krizhevsky2012imagenet} as the activation function. DropOut \cite{DropOut} was applied to this layer with a ratio of 0.3 (70\% of the activations were kept). A final fully-connected layer was connected, providing a 36-element output. This layer utilized the sigmoid function for its activations.

  \section{Training}\label{training}

    Since the output of the network was simply an array in which 0 indicated ``no object detected'' and 1 indicated ``object detected,'' we treated the region-proposal problem as a multi-label classification problem and used the cross-entropy loss as our cost function.

    For optimization we used stochastic gradient descent with momentum \(\mu = 0.9\). We found the best base learning rate to be \(\alpha = 0.0001\). Since our datasets were relatively small we set weight decay to a fairly high setting of 0.05 to avoid over-fitting and promote generalization. We found that this higher-than-normal setting resulted in the best testing performance. We tested the network after 300,000 iterations of training. This resulted in 7 epochs of training for the Monterey Bay Webcam Dataset and 23 epochs of training for the \ac{3DPeS} dataset.

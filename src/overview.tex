\chapter{Methodology}\label{methodology}

We used the Caffe framework \cite{caffe} to implement a relatively
simple network, shown in Figure \ref{fig:network_diagram}.

\begin{figure}
  \centering
    \includegraphics[width=1.0\textwidth]{figs/cnn_diagram.png}
  \caption{Convolutional neural network used to detect moving objects.}
  \label{fig:network_diagram}
\end{figure}

The network is given 10 ordered frames from a video sequence taken
uniformly from a 30-frame segment. In other words, between each frame
provided to the network 2 frames are dropped.

The network is trained to detect objects located in the 6th frame
provided to it. This allows the network to look 0.75 seconds into the
past and 0.45 seconds into the future to assist with detection. We
compare these results with a version of the network given only a single
frame.

\section{Training the Network}\label{training-the-network}

Each of the datasets were annotated by humans with a bounding box
surrounding each target object. The output of the network was a simple
36-element array where each element corresponded to one region in a 6x6
element grid. The network was trained in a ``one-hot'' manner; if an
object appeared in a region of the image, the corresponding element of
the output array should be 1. There may be no objects present in an
image, in which case the network should output an array where every
element is 0. There may be multiple target objects in an image,
appearing in different regions. In this case the network was trained to
output an array with multiple 1's. If two objects appeared in the same
region the network would only set the element corresponding to that
region to 1.

Since the datasets were annotated with bounding boxes and not a 1-hot
occupancy grid, the data layer in the network translated the bounding
boxes to grid values by a simple intersection function: if a region was
covered 3\% by a bounding box (which excluded moments when only the edge
of a bounding box occupied a region but the object did not) it was
deemed to include an object, and was labeled with a `1'. Otherwise the
element was labeled `0'.

\subsection{A Simple Dataset}\label{a-simple-dataset}

Before training the network on difficult, noisy scenes we evaluated the
network's performance using a subset of the 3D People Surveillance
Dataset (3DPeS) \cite{visor_dataset}, a series of videos captured by
fixed cameras of pedestrians moving against a static background. Since
3DPeS did not contain bounding-box annotations of the pedestrians, we
annotated the video sequences using the Video Annotation Tool from
Irvine, California (VATIC) \cite{vatic}. We annotated and used 45
sequences comprising of 7 camera angles. The sequences contain an
average of 250 frames.

\begin{figure}
  \centering
    \includegraphics[width=0.3\textwidth]{figs/3dpes_examples/ex1.jpg}
    \includegraphics[width=0.3\textwidth]{figs/3dpes_examples/ex2.jpg}
    \includegraphics[width=0.3\textwidth]{figs/3dpes_examples/ex3.jpg}
  \caption{Examples of scenes from 3DPeS.}
  \label{fig:3dpes_example}
\end{figure}

\subsection{A Noisy Dataset}\label{a-noisy-dataset}

We chose webcam footage of the Monterey Bay as a ``noisy'' dataset, and
trained the network to detect birds flying across the frame. While the
camera position was static in these sequences, the background was
continuously changing due to sea state and surface debris. The target
objects were moving in every sequence. A single frame rarely has enough
information to make an accurate detection. Figure
\ref{fig:mb_dataset_examples} shows three frames from the dataset; each
frame has a positive object in it.

\begin{figure}
  \centering
    \includegraphics[width=0.3\textwidth]{figs/mb_dataset_examples/ex1.jpg}
    \includegraphics[width=0.3\textwidth]{figs/mb_dataset_examples/ex2.jpg}
    \includegraphics[width=0.3\textwidth]{figs/mb_dataset_examples/ex3.jpg}
  \caption{Still frames from the Monterey Bay Dataset. Each frame has a positive object in it.}
  \label{fig:mb_dataset_examples}
\end{figure}

Training the network on this dataset directly resulted in poor results.
While the network was able to overfit to the data it was unable to
generalize to new sets, even when using high weight decay. We suspected
that the network was finding a suboptimal local minima. To overcome this
obstacle we trained the network on a series of easier problems, and then
fine-tuned the weights after each training session.

In the first pretraining step we trained the network to detect a large
object moving against a background of ocean waves. The object was
colored either black or white and moved at a random constant speed
following a linear path in a random direction. Throughout each sequence
the object was only rendered 50\% of the time; the network had to learn
to use multiple frames to determine the location of the object. This
dataset was generated synthetically and included 400 sequences,
averaging 100 frames each.

\begin{figure}
  \centering
    \includegraphics[width=0.3\textwidth]{figs/buoy_examples/ex1.jpg}
    \includegraphics[width=0.3\textwidth]{figs/buoy_examples/ex2.jpg}
    \includegraphics[width=0.3\textwidth]{figs/buoy_examples/ex3.jpg}
  \caption{Sequence of frames from the synthetic ``buoy-shape'' dataset used in the first pretraining step.}
  \label{fig:buoy_dataset_examples}
\end{figure}

The weights trained on the buoy dataset were used as a starting point
when training the network on a synthetic ``bird-shape'' dataset. This
dataset was generated identically to the ``buoy-shape'' dataset expect
that the shapes were much smaller. A small point of either black or
white, only a few pixels across, was rendered moving in a random
direction at a constant speed against a background of ocean waves. Like
the synthetic buoys, the synthetic bird shapes were only rendered in
50\% of the frames; the network had to use multiple frames to determine
the precise location of the object.

\begin{figure}
  \centering
    \includegraphics[width=0.3\textwidth]{figs/bird_examples/ex1.jpg}
    \includegraphics[width=0.3\textwidth]{figs/bird_examples/ex2.jpg}
    \includegraphics[width=0.3\textwidth]{figs/bird_examples/ex3.jpg}
  \caption{Sequence of frames from the synthetic ``bird-shape'' dataset used in the first pretraining step.}
  \label{fig:buoy_dataset_examples}
\end{figure}
